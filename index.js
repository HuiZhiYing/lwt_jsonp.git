//这个是老的jsonp函数
//要先 npm install jsonp
const originJsonp = require('jsonp');

module.exports = function jsonp(url, data, option) {
    url += (url.indexOf('?') < 0 ? '?' : '&') + param(data)

    //这个封装成一个promise
    return new Promise((resolve, reject) => {
        originJsonp(url, option, (err, data) => {
            if (!err) {
                resolve(data)
            } else {
                reject(err)
            }
        })
    })
}

//这个是对url的处理函数.
//data是一个对象,将对象的键值对给集成到url当中去
function param(data) {
    let url = '';
    for (var k in data) {
        let value = data[k] !== undefined ? data[k] : ''
        url += '&' + k + '=' + encodeURIComponent(value)
    }
    return url ? url.substring(1) : '';
}

